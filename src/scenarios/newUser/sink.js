"use strict";

const stan = require('node-nats-streaming')
  .connect('broker', 'sink', { url: "nats://broker:4222" });

const optReplayAll = stan
  .subscriptionOptions()
  .setDeliverAllAvailable();

const users = [];

const userIDTopicHandler = msg => {
  const { type, payload } = JSON.parse(msg.getData()),
    attr = type.replace(/(User|Updated)/g, '').toLowerCase(),
    topic = msg.getSubject(),
    id = Number.parseInt(topic.replace('user.',''));

  users
    .filter(u => u.id === id)
    .forEach(u => u[attr] = payload);
  
  console.log(`[${topic}] ${msg.getData()}`);
};

stan.on('connect', () => {
  const subs = {
    newUser: stan.subscribe('new.user', optReplayAll),
    control: stan.subscribe('control', optReplayAll)
  };

  subs.newUser.on('message', msg => {
    const event = JSON.parse(msg.getData()),
      topic = `user.${event.payload}`;

    users.push({ id: event.payload });

    subs[topic] = stan.subscribe(topic, optReplayAll);
    subs[topic].on('message', userIDTopicHandler);

    console.log(`[new.user] ${msg.getData()}`);
  });

  subs.control.on('message', () => console.log(users));
});

stan.on(`close`, () => process.exit());
