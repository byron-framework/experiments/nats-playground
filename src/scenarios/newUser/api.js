"use strict";

const faker = require('faker');
const stan = require('node-nats-streaming')
  .connect('broker', 'api', { url: "nats://broker:4222" });


const {
  UserCreatedEvent,
  UserEmailUpdatedEvent,
  UserNameUpdatedEvent
} = require("../events.js");


faker.seed(123);

const pubCallback = (err, guid) => {
  if (err) console.log(`[Failed to publish]\n${err}`);
};

stan.on('connect', () => {
  let i, user, event;
  const N = 15,
    events = [];

  setTimeout(() => stan.publish('control', 'bla', pubCallback), 5000);

  for (i = 0; i < N; i++) {
    user = {
      id: i+1,
      name: faker.name.firstName().toLowerCase(),
      email: faker.internet.email()
    }

    events.push(new UserCreatedEvent(user));
    events.push(new UserEmailUpdatedEvent(user));
    events.push(new UserNameUpdatedEvent(user));

    while (events.length) {
      event = events.shift();
      stan.publish(
        event.getTopic(),
        event.toString(),
        pubCallback
      )
    }
  }
});
