"use strict";

class Event {
  constructor(type, payload, topic) {
    this.type = type;
    this.payload = payload;
    this.topic = topic;
  }

  getTopic() {
    return this.topic;
  }

  toString() {
    const copy = JSON.parse(JSON.stringify(this));
    delete copy.topic;
    return JSON.stringify(copy);
  }
}

class UserCreatedEvent extends Event {
  constructor(user) {
    super("UserCreated", user.id, `new.user`);
  }
}

class UserEmailUpdatedEvent extends Event {
  constructor(user) {
    super("UserEmailUpdated", user.email, `user.${user.id}`);
  }
}

class UserNameUpdatedEvent extends Event {
  constructor(user) {
    super("UserNameUpdated", user.name, `user.${user.id}`);
  }
}

class UserDeletedEvent extends Event {
  constructor(user) {
    super("UserDeleted", user.id, `delete.user`);
  }
}

module.exports = {
  UserCreatedEvent,
  UserEmailUpdatedEvent,
  UserNameUpdatedEvent,
  UserDeletedEvent
};
