"use strict";

const faker = require('faker');
const stan = require('node-nats-streaming')
  .connect('broker', 'api', { url: "nats://broker:4222" });

const {
  UserCreatedEvent,
  UserEmailUpdatedEvent,
  UserNameUpdatedEvent
} = require("../events.js");


faker.seed(123);

const pubCallback = (err, guid) => {
  if (err) console.log(`[Failed to publish]\n${err}`);
};

stan.on('connect', () => {
  let i, user, event, r, ui, q,
      t = 0,
      dt = 750;

  const N = 15,
    events = [],
    users = [];

  for (i = 0; i < N; i++) {
    user = {
      id: i+1,
      name: faker.name.firstName(),
      email: faker.internet.email().toLowerCase()
    }

    users.push(user);

    events.push(new UserCreatedEvent(user));
    events.push(new UserEmailUpdatedEvent(user));
    events.push(new UserNameUpdatedEvent(user));

    r = Math.random() * users.length;
    for (let j = 0; j < r; j++) {
      ui = Math.floor(Math.random() * (users.length - 1));
      user = users[ui];
      q = Math.random();

      if (q < 2.0/3) {
        user.name = faker.name.firstName();
        events.push(new UserNameUpdatedEvent(user));
      }

      if (q > 1.0/3) {
        user.email = faker.internet.email().toLowerCase();
        events.push(new UserEmailUpdatedEvent(user));
      }
    }

    while(events.length) {
      event = events.shift();
      stan.publish(
        event.getTopic(),
        event.toString(),
        pubCallback
      );
    }
  }
});
