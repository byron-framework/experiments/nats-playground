"use strict";

const faker = require('faker');
const stan = require('node-nats-streaming')
  .connect('broker', 'api', { url: "nats://broker:4222" });


const {
  UserCreatedEvent,
  UserEmailUpdatedEvent,
  UserNameUpdatedEvent,
  UserDeletedEvent
} = require("../events.js");


faker.seed(123);

const pubCallback = (err, guid) => {
  if (err) console.log(`[Failed to publish]\n${err}`);
};

let users = [];

stan.on('connect', () => {
  let i, user, event;
  const N = 15,
    events = [];

  for (i = 0; i < N; i++) {
    user = {
      id: i+1,
      name: faker.name.firstName(),
      email: faker.internet.email().toLowerCase()
    }

    users.push(user);

    events.push(new UserCreatedEvent(user));
    events.push(new UserEmailUpdatedEvent(user));
    events.push(new UserNameUpdatedEvent(user));

    while (events.length) {
      event = events.shift();
      stan.publish(
        event.getTopic(),
        event.toString(),
        pubCallback
      )
    }
  }

  for (i = 0; i < N/5; i++) {
    let ui = Math.floor(Math.random() * users.length);
    user = users[ui];
    users = users.filter(u => u.id !== user.id);
    event = new UserDeletedEvent(user);
    stan.publish(
      event.getTopic(),
      event.toString(),
      pubCallback
    );
  }
});

setTimeout(() => console.log(users), 5000);
