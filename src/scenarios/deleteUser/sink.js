"use strict";

const stan = require('node-nats-streaming')
  .connect('broker', 'sink', { url: "nats://broker:4222" });

const optReplayAll = stan
  .subscriptionOptions()
  .setDeliverAllAvailable();

let users = [];

const userIDTopicHandler = msg => {
  const { type, payload } = JSON.parse(msg.getData()),
    attr = type.replace(/(User|Updated)/g, '').toLowerCase(),
    topic = msg.getSubject(),
    id = Number.parseInt(topic.replace('user.',''));

  users
    .filter(u => u.id === id)
    .forEach(u => u[attr] = payload);
};

stan.on('connect', () => {
  const subs = {
    newUser: stan.subscribe('new.user', optReplayAll)
  };

  subs.newUser.on('message', msg => {
    const { type, payload } = JSON.parse(msg.getData()),
      topic = `user.${payload}`;

    users.push({ id: payload });

    subs[topic] = stan.subscribe(topic, optReplayAll);
    subs[topic].on('message', userIDTopicHandler);
  });

  subs.deleteUser = stan.subscribe(`delete.user`, optReplayAll);
  subs.deleteUser.on('message', msg => {
    const { type, payload } = JSON.parse(msg.getData()),
          topic = `user.${payload}`;

    users = users.filter(u => u.id !== Number.parseInt(payload));
  });
});

stan.on(`close`, () => process.exit());
setTimeout(() => console.log(users), 5000);
