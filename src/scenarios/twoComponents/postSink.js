"use strict";

const stan = require('node-nats-streaming')
  .connect('broker', 'postSink', { url: "nats://broker:4222" });

const optReplayAll = stan
  .subscriptionOptions()
  .setDeliverAllAvailable();

const users = [];

const userIDTopicHandler = msg => {
  const { type, payload } = JSON.parse(msg.getData()),
    attr = type.replace(/(User|Updated)/g, '').toLowerCase(),
    topic = msg.getSubject(),
    id = Number.parseInt(topic.replace('user.',''));

  users
    .filter(u => u.id === id)
    .forEach(u => u[attr] = payload);
};

stan.on('connect', () => {
  const subs = {
    newUser: stan.subscribe('new.user', optReplayAll)
  };

  subs.newUser.on('message', msg => {
    const event = JSON.parse(msg.getData()),
      topic = `user.${event.payload}`;

    users.push({ id: Number.parseInt(event.payload) });
  });
});

stan.on(`close`, () => process.exit());

setTimeout(() => console.log(users), 5000);
