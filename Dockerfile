FROM node:alpine

WORKDIR /usr/src/app

RUN npm i -g nodemon nats-cli

COPY package* ./

RUN npm install

COPY . ./

CMD nodemon src/scenarios/newUser/api.js
